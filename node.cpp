#include "node.h"

class Node {
  int ipaddr, lastBeat, status, isCoordinator;
  public:
    void setAddr();
    void openConnection();
    void updateBeat();
    void setStatus();
    void setCoord();
};

void Node::setAddr() {
  /* TODO
    set addr info for node
  */
}

void Node::openConnection() {
  /* TODO
    open a new connection to a specified ip
  */
}

void Node::udpateBeat() {
  /* TODO
    update internal heartbeat vals --> used by failure detector
  */
}

void Node::setStatus() {
  /* TODO
    set node's status --> ACTIVE/INACTIVE/SUSPICIOUS
  */
}

void Node::setCoord() {
  /* TODO
    set node as coordinator --> internal behavior?
  */
}
