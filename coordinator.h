#ifndef COORDINATOR_H
#define COORDINATOR_H

#define BUFFSIZE        0x400
#define INIT            0x0000
#define ACK             0x0001
#define ADDJOBS         0x0002
#define ENDROUND        0x0003
#define SAVE            0x0004
#define ELECT           0x0005

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#endif
