#include "failureDetector.h"

void gossipRandom() {
  /* TODO
    select k random processes to gossip to
  */
}

void checkStale() {
  /* TODO
    check if a process has not been heard from within Tfail (direct/indirect) --> mark as failed
    check if a process has not been heard form within Tcleanup (direct/indirect) --> delete node
    may need to tie Tcleanup into informing coordinator so it can reassign tasks
  */
}
