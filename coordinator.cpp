// my second program in C++
#include "coordinator.h"

void receive () {
  char *buff = NULL;
  unsigned short int opcode;

  memcpy(buff, &opcode, sizeof(unsigned short int));

  if (opcode == ACK) {
    /* TODO
      ACK handler code --> mark worker node as active, status = waiting for ENDROUND
    */
  }

  else if (opcode == ENDROUND) {
    /* TODO
      ENDROUND handler code --> mark worker as completed for this round, thread will check if ready to move on
    */
  }

}

void send () {
  /* TODO
    send necessary msgs
  */
}

void updateRound() {
  /* TODO
    check if all nodes have completed the round --> reset all statuses and send NEWROUND msg
  */
}

void saveResults() {
  /* TODO
    periodically tell worker nodes to save their results for tolerance
  */
}

int main ()
{
  std::cout << "Hello World! \n";
  std::cout << "I'm a C++ program \n";
}
