#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SERVER_PORT   5432
#define MAX_PENDING   5
#define MAX_LINE      256

struct node {
  /* may want to pack these later? */
  int socket_id;
  int send_count;
  int recv_count;
  unsigned long last_heartbeat;
}node_t;

struct node *neighborNodes[3];
int id;

void bootstrap() {
  /* talk to designated introducer from config file */

}

void createReceiver() {
  /* dgram receiver for heartbeats */
}

void createSender() {
  /* dgram sender for heartbeats */
}

void sendHeartbeat() {
  /* send hb to all others then take sleep */
  char buf[MAX_LINE];
  int i;
  int len;
  for (i = 0; i < sizeof(neighborNodes)/sizeof(struct node); i++) {
    if (i == id) {
      continue;
    }
    bzero((char *)buf, sizeof(buf));
    send(neighborNodes[i]->socket_id, buf, len, 0);
  }
  sleep(20);
}

void checkHeartbeat() {
  /* recv hb, check if any stale hb */
}


int main() {

}
