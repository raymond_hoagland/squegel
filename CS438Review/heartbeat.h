#ifndef HEARTBEAT_H
#define HEARTBEAT_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define PORT          5432
#define PORT_STR      "5432"
#define MAX_LINE      256
#define ACK_INT_MSG   "ACKINT"
#define INTRO_MSG     "INTSYN"
#define HEART_MSG     "HEABEA"
#define MSG_LEN       6
#define STATUS_INT    1
#define STATUS_DONE   2
#define STATUS_FAIL   3
#define STATUS_CLEAN  4
#define ADDR_LEN      15
#define RAND_GOSSIP   3
#define T_FAIL        15
#define T_CLEAN       30

struct node {
  int send_count;
  int recv_count;
  unsigned long last_heartbeat;
  struct sockaddr_in sin;
  int status;
  char local_addr[15];
  struct sockaddr_storage from;
  unsigned int from_len;
};

extern int neighbor_node_count;
extern int init;
extern int socket_id;
extern int *random_selections;
extern struct node *neighbor_nodes;
extern struct node *introducer_node;
extern char local_addr[MAX_LINE];

void setup();
void pickRandom();
void constructSin(int idx);
void handleIntroduction(char *recv_buf, char *addr_buf);
void constructHeartbeat(char *send_buf);
void *sendHeartbeat(void *arg);
void *recvHeartbeat(void *arg);
void *checkHeartbeat(void *arg);


#endif
