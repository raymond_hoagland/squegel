#ifndef INTRODUCER_H
#define INTRODUCER_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define PORT_STR        "5432"
#define MAX_LINE        256
#define ACK_INT_MSG     "ACKINT"
#define INTRO_MSG       "INTSYN"
#define MSG_LEN         6
#define STATUS_INT      1
#define STATUS_DONE     2
#define ADDR_LEN        15

struct node {
  int status;
  char local_addr[15];
  struct sockaddr_storage from;
  unsigned int from_len;
};

extern int recv_socket;
extern int init;
extern int neighbor_node_count;
extern unsigned int from_len;
extern struct sockaddr_storage from;
extern struct node *neighbor_nodes;

void setup();
void constructPacket (char *send_buf, int *pos);
void introduce(char *recv_buf, char *local_addr, int recvd);
void *sendInfo(void *arg);
void *recvInfo(void *arg);

#endif
