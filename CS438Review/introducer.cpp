#include "introducer.h"

int recv_socket, init = 1, neighbor_node_count = 0;
unsigned int from_len = sizeof(struct sockaddr_storage);
struct sockaddr_storage from;
struct node *neighbor_nodes = NULL;

void setup() {
  /* create/bind socket */
  struct addrinfo hints;
  struct addrinfo *serverinfo;
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  int status = getaddrinfo(NULL, PORT_STR, &hints, &serverinfo);
  recv_socket = socket(serverinfo->ai_family, serverinfo->ai_socktype, serverinfo->ai_protocol);
  status = bind(recv_socket, serverinfo->ai_addr, serverinfo->ai_addrlen);
  freeaddrinfo(serverinfo);
}

void constructPacket (char *send_buf, int *pos) {
  int i, tmp_count;

  /* construct introduction packet */
  bzero(send_buf, MAX_LINE);
  bcopy(ACK_INT_MSG, send_buf, MSG_LEN);
  tmp_count = htons(neighbor_node_count);
  memcpy(send_buf+MSG_LEN, (void *)(&(tmp_count)), sizeof(int));
  *pos = MSG_LEN + sizeof(int);
  for (i = 0; i < neighbor_node_count; i++) {
    memcpy(send_buf+MSG_LEN+sizeof(int)+(i*ADDR_LEN), neighbor_nodes[i].local_addr, ADDR_LEN);
    *pos += ADDR_LEN;
  }
}

void introduce(char *recv_buf, char *local_addr, int recvd) {
  int idx = -1, i = 0;

  bzero(local_addr, MAX_LINE);
  bcopy(recv_buf+MSG_LEN, local_addr, recvd-MSG_LEN);
  for (i = 0; i < neighbor_node_count; i++) {
    if (strncmp(neighbor_nodes[i].local_addr, local_addr, strlen(local_addr)) == 0) {
      idx = i;
    }
  }

  /* unknown node joining the system */
  if (idx == -1) {
    if (neighbor_node_count == 0) {
      neighbor_nodes = (struct node *)malloc(sizeof(struct node));
    }
    else {
      realloc(neighbor_nodes, sizeof(struct node)*neighbor_node_count+1);
    }
    memcpy(neighbor_nodes[neighbor_node_count].local_addr, local_addr, ADDR_LEN);
    memcpy(&(neighbor_nodes[neighbor_node_count].from), &from, sizeof(sockaddr_storage));
    memcpy(&(neighbor_nodes[neighbor_node_count].from_len), &from_len, sizeof(unsigned int));
    idx = neighbor_node_count;
    neighbor_node_count += 1;
  }

  /* introduction request so set status */
  neighbor_nodes[idx].status = STATUS_INT;
}

void *sendInfo(void *arg) {
  /* send introduction info */
  char send_buf[MAX_LINE];
  int i, pos = 0;
  while (1) {
    constructPacket(send_buf, &pos);
    for (i = 0; i < neighbor_node_count; i++) {
        if (neighbor_nodes[i].status == STATUS_INT) {
          /* send intro packet to neighbor */
          int k = sendto(recv_socket, send_buf, pos, 0, (struct sockaddr *)(&(neighbor_nodes[i].from)), *(&(neighbor_nodes[i].from_len)));
          neighbor_nodes[i].status = STATUS_DONE;
          printf("%d", k);
        }
        else {
          /* TODO other things... */
        }
    }
    usleep(50);
  }
}

void *recvInfo(void *arg) {
  /* receive intro requests from new nodes */
  char recv_buf[MAX_LINE];
  char local_addr[MAX_LINE];
  int recvd, idx, i;
  while (1) {
    bzero(recv_buf, MAX_LINE);
    bzero(local_addr, MAX_LINE);
    recvd = recvfrom(recv_socket, recv_buf, MAX_LINE, 0, (struct sockaddr *)(&from), &from_len);

    /* request for introduction */
    if (strncmp(recv_buf, INTRO_MSG, MSG_LEN) == 0) {
      introduce(recv_buf, local_addr, recvd);
    }

    printf("%s\n", recv_buf);
    usleep(20);
  }
}

int main() {
  setup();
  pthread_t sendThread;
  pthread_t recvThread;
  int i = 5;

  pthread_create(&sendThread, NULL, sendInfo, (void *)(&i));
  pthread_create(&recvThread, NULL, recvInfo, (void *)(&i));
  while (1);
}
