#include "heartbeat.h"

int neighbor_node_count = 0, init = 1, socket_id = -1;
int *random_selections = (int *)malloc(sizeof(int)*RAND_GOSSIP);
struct node *neighbor_nodes = NULL, *introducer_node = NULL;
char local_addr[MAX_LINE];

void setup() {
  /* talk to designated introducer from config file */
  FILE *config_file;
  char buf[MAX_LINE];
  char addr[MAX_LINE];
  struct hostent *hp;

  /* read the config file for setup */
  config_file = fopen("config.txt", "r");
  if (config_file != NULL) {
    bzero(buf, MAX_LINE);
    bzero(addr, MAX_LINE);
    while(fscanf(config_file, "%s %s", buf, addr) > 0) {
      if (strncmp(buf, "Introducer:", 11) == 0) {
        /* fill in introducer info */
        introducer_node = (struct node *)malloc(sizeof(struct node));
        hp = gethostbyname(addr);
        bzero((char *)&introducer_node->sin, sizeof(introducer_node->sin));
        introducer_node->sin.sin_family = AF_INET;
        bcopy(hp->h_addr, (char *)&introducer_node->sin.sin_addr, hp->h_length);
        introducer_node->sin.sin_port = htons(PORT);

        /* create/bind the socket */
        struct addrinfo hints;
        struct addrinfo *serverinfo;
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_DGRAM;
        hints.ai_flags = AI_PASSIVE;

        int status = getaddrinfo(NULL, PORT_STR, &hints, &serverinfo);
        socket_id = socket(serverinfo->ai_family, serverinfo->ai_socktype, serverinfo->ai_protocol);
        status = bind(socket_id, serverinfo->ai_addr, serverinfo->ai_addrlen);
        bzero(introducer_node->local_addr, MAX_LINE);
        bcopy(addr, introducer_node->local_addr, MAX_LINE);
      }
      else if (strncmp(buf, "Local:", 6) == 0) {
        /* identify local settings */
        if (introducer_node != NULL) {
          bzero(local_addr, MAX_LINE);
          bcopy(addr, local_addr, MAX_LINE);
        }
      }
    }
  }
  else {
    printf("Err opening config file.\n");
  }
}

void pickRandom() {
  int num, i;

  srand (time(NULL));
  for (i = 0; i < RAND_GOSSIP; i++) {
    random_selections[i] = rand() % neighbor_node_count;
  }
}

void constructSin(int idx) {
  char addr[MAX_LINE];
  struct hostent *hp;

  /* sin for each node that needs to be talked to --> reuse same socket id  */
  memcpy(addr, neighbor_nodes[idx].local_addr, ADDR_LEN);
  hp = gethostbyname(addr);
  bzero((char *)&neighbor_nodes[idx].sin, sizeof(neighbor_nodes[idx].sin));
  neighbor_nodes[idx].sin.sin_family = AF_INET;
  bcopy(hp->h_addr, (char *)&neighbor_nodes[idx].sin.sin_addr, hp->h_length);
  neighbor_nodes[idx].sin.sin_port = htons(PORT);
}

void handleIntroduction(char *recv_buf, char *addr_buf) {
  int i = 0, idx = -1, k = 0, num_neighbors = 0;

  init = 0;
  memcpy(&num_neighbors, recv_buf+MSG_LEN, sizeof(int));
  num_neighbors = ntohs(num_neighbors);
  printf("%d\n", num_neighbors);
  for (i = 0; i < num_neighbors; i++) {
    bzero(addr_buf, MAX_LINE);
    memcpy(addr_buf, recv_buf+MSG_LEN+sizeof(int)+(ADDR_LEN*i), ADDR_LEN);

    /* filter out own address */
    if (strncmp(addr_buf, local_addr, strlen(local_addr)) == 0) {
      printf("Ignoring my address!\n");
      continue;
    }

    /* filter out existing nodes */
    idx = -1;
    for (k = 0; k < neighbor_node_count; k++) {
      if (strncmp(neighbor_nodes[k].local_addr, addr_buf, strlen(local_addr)) == 0) {
        idx = k;
      }
    }
    if (idx != -1) {
      continue;
    }

    if (neighbor_node_count == 0) {
      neighbor_nodes = (struct node *)malloc(sizeof(struct node));
    }
    else {
      realloc(neighbor_nodes, sizeof(struct node)*neighbor_node_count+1);
    }
    printf("Adding new node: %s\n", local_addr);
    memcpy(neighbor_nodes[neighbor_node_count].local_addr, local_addr, strlen(local_addr));
    constructSin(neighbor_node_count);
    neighbor_node_count += 1;
  }
}

void constructHeartbeat(char *send_buf) {
  int i, pos;
  unsigned long lhb;

  /* TODO do we still send nodes that are STATUS_FAIL? */
  pos = 0;
  memcpy(send_buf, HEART_MSG, MSG_LEN);
  pos += MSG_LEN;
  for (i = 0; i < neighbor_node_count; i++) {
    memcpy(send_buf+pos, neighbor_nodes[i].local_addr, ADDR_LEN);
    pos += ADDR_LEN;
    lhb = htonl(neighbor_nodes[i].last_heartbeat);
    memcpy(send_buf+pos, &(lhb), sizeof(unsigned long));
    pos += sizeof(unsigned long);
  }
}

void *sendHeartbeat(void *arg) {
  /* send HB */
  char send_buf[MAX_LINE];
  int i, idx;

  while (1) {
    bzero(send_buf, MAX_LINE);
    if (init == 1) {
      /* TODO make sure all data is sent */
      bcopy(INTRO_MSG, send_buf, MSG_LEN);
      bcopy(local_addr, send_buf+MSG_LEN, strlen(local_addr));
      sendto(socket_id, send_buf, strlen(send_buf), 0, (sockaddr *)(&(introducer_node->sin)), sizeof(introducer_node->sin));
    }
    else {
      /* pick k randoms */
      constructHeartbeat(send_buf);
      pickRandom();
      for (i = 0; i < RAND_GOSSIP; i++) {
        idx = random_selections[i];
        sendto(socket_id, send_buf, strlen(send_buf), 0, (sockaddr *)(&(neighbor_nodes[idx].sin)), sizeof(neighbor_nodes[idx].sin));
        usleep(10);
      }
    }
    usleep(100);
  }
}

void *recvHeartbeat(void *arg) {
  /* check HB */
  char recv_buf[MAX_LINE], addr_buf[MAX_LINE];
  struct sockaddr_storage *from;
  unsigned int *from_len;

  while (1) {
    bzero(recv_buf, MAX_LINE);
    recvfrom(socket_id, recv_buf, MAX_LINE, 0, (struct sockaddr *)from, from_len);

    /* ACK for introduction */
    if (strncmp(recv_buf, ACK_INT_MSG, MSG_LEN) == 0) {
      handleIntroduction(recv_buf, addr_buf);
    }
    usleep(20);
  }
}

void *checkHeartbeat(void *arg) {
  /* check for stale nodes */
  int i;

  while(1) {
    for (i = 0; i < neighbor_node_count; i++) {
      if (difftime(time(NULL), neighbor_nodes[i].last_heartbeat) > T_CLEAN) {
        neighbor_nodes[i].status = STATUS_CLEAN;
        /* TODO delete the node! */
      }
      else if (difftime(time(NULL), neighbor_nodes[i].last_heartbeat) > T_FAIL) {
        neighbor_nodes[i].status = STATUS_FAIL;
      }
    }
    usleep(30);
  }
}

int main() {
  int i = 4;
  pthread_t send_thread;
  pthread_t recv_thread;
  pthread_t check_thread;

  setup();
  pthread_create(&send_thread, NULL, sendHeartbeat, (void *)(&i));
  pthread_create(&recv_thread, NULL, recvHeartbeat, (void *)(&i));
  pthread_create(&check_thread, NULL, checkHeartbeat, (void *)(&i));
  while(1);
}
